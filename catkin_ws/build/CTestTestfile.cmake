# CMake generated Testfile for 
# Source directory: /home/nga/catkin_ws/src
# Build directory: /home/nga/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("mybot")
subdirs("mybot_control")
subdirs("mybot_description")
subdirs("mybot_gazebo")
subdirs("mybot_navigation")
subdirs("turtlebot3/turtlebot3")
subdirs("turtlebot3_msgs")
subdirs("turtlebot3/turtlebot3_navigation")
subdirs("turtlebot3_simulations/turtlebot3_simulations")
subdirs("followbot")
subdirs("test")
subdirs("multi_robot")
subdirs("turtlebot3/turtlebot3_bringup")
subdirs("turtlebot3/turtlebot3_example")
subdirs("turtlebot3_simulations/turtlebot3_fake")
subdirs("turtlebot3_simulations/turtlebot3_gazebo")
subdirs("turtlebot3/turtlebot3_slam")
subdirs("turtlebot3/turtlebot3_teleop")
subdirs("turtlebot3/turtlebot3_description")
